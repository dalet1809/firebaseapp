import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'RegistroWidget.dart';
import '../config/globales.dart' as globales;


class InicioSesionWidget extends StatefulWidget {
  @override
  InicioSesionEstado createState() => InicioSesionEstado();
}

class InicioSesionEstado extends State<InicioSesionWidget> {
  final usuarioControlador = TextEditingController();
  final claveControlador = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(globales.sesionActiva) {
      //Navigator.of(this.context).pushNamed('/iniciosesion');
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushReplacementNamed("/");
      });
    }
  }

  @override
  Widget build(BuildContext contexto) {
    return Scaffold(
      appBar: AppBar(
        title: Text('INICIO SESION'),
//        leading: IconButton(
//            icon: Icon(Icons.menu, semanticLabel: '[INICIOSESION] menu'),
//            onPressed: () {
//              print('Menu');
//            }),

      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 120.0),
            TextField(
              controller: usuarioControlador,
              decoration: InputDecoration(filled: false, labelText: 'Usuario'),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: claveControlador,
              decoration: InputDecoration(filled: false, labelText: 'Clave'),
              obscureText: true,
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: Text('REGISTRO'),
                  onPressed: () {
                    usuarioControlador.clear();
                    claveControlador.clear();
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegistroWidget()));
                  },
                ),
                FlatButton(
                  child: Text('INICIAR SESION'),
                  onPressed: () {
                    usuarioControlador.clear();
                    claveControlador.clear();

                    globales.sesionActiva = true;
                    Navigator.of(context).pushReplacementNamed("/");
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
