import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import '../config/globales.dart' as globales;

class UsuariosWidget extends StatefulWidget {


  @override
  UsuariosState createState() => UsuariosState();
}

class UsuariosState extends State<UsuariosWidget> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(!globales.sesionActiva) {
      //Navigator.of(this.context).pushNamed('/iniciosesion');
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushReplacementNamed("/iniciosesion");
      });
    }
  }

  @override
  Widget build(BuildContext contexto) {
    return Scaffold(
      appBar: AppBar(
        title: Text('USUARIOS'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home, semanticLabel: 'Inicio'),
            onPressed: () {
              print('[INICIOSESINO] boton usuarios');
              Navigator.of(context).pushReplacementNamed("/");
            },
          ),
          IconButton(
            icon:
            Icon(Icons.power_settings_new, semanticLabel: 'Cerrar sesión'),
            onPressed: () {
              print('[INICIOSESINO] boton cerrar sesion');
              globales.sesionActiva = false;
              Navigator.of(context).pushReplacementNamed("/iniciosesion");
            },
          )
        ],
      ),
      body: Container()

    );
  }
}
