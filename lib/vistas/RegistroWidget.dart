import 'package:flutter/material.dart';

class RegistroWidget extends StatefulWidget {
  @override
  RegistroEstado createState() => RegistroEstado();
}

class RegistroEstado extends State<RegistroWidget> {
  final nombreControlador = TextEditingController();
  final usuarioControlador = TextEditingController();
  final claveControlador = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Firebase'),


      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 20.0),
            TextField(
              controller: nombreControlador,
              decoration: InputDecoration(filled: false, labelText: 'Nombre'),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: usuarioControlador,
              decoration: InputDecoration(filled: false, labelText: 'Usuario'),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: claveControlador,
              decoration: InputDecoration(filled: false, labelText: 'Clave'),
              obscureText: true,
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: Text('ENVIAR DATOS'),
                  onPressed: () {
                    nombreControlador.clear();
                    usuarioControlador.clear();
                    claveControlador.clear();
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
