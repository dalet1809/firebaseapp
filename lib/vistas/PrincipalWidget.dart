import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import '../config/globales.dart' as globales;

class PrincipalWidget extends StatefulWidget {
  @override
  PrincipalEstado createState() => PrincipalEstado();
}

class PrincipalEstado extends State<PrincipalWidget> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(!globales.sesionActiva) {
      //Navigator.of(this.context).pushNamed('/iniciosesion');
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushReplacementNamed("/iniciosesion");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: Text('FIREBASE APP'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.people, semanticLabel: 'Usuarios'),
              onPressed: (){
                print('[INICIOSESINO] boton usuarios');
                Navigator.of(context).pushReplacementNamed("/usuarios");
              },
            ),
            IconButton(
              icon: Icon(Icons.power_settings_new, semanticLabel: 'Filtrar'),
              onPressed: (){
                print('[INICIOSESINO] boton cerrar sesion');
                globales.sesionActiva = false;
                Navigator.of(context).pushReplacementNamed("/iniciosesion");
              },
            )
          ],
        ),
        body: Container());
  }
}
