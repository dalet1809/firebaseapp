import 'package:flutter/foundation.dart';

class Usuario {
  final String indice;
  final String nombre;
  final String correo;
  final String clave;
  final String fecha;

  const Usuario(
      {@required this.indice,
      @required this.nombre,
      @required this.correo,
      @required this.clave,
      @required this.fecha})
      : assert(indice != null),
        assert(nombre != null),
        assert(correo != null),
        assert(clave != null),
        assert(fecha != null);
}
