import 'package:flutter/material.dart';
import 'vistas/InicioSesionWidget.dart';
import 'vistas/UsuariosWidget.dart';
import 'vistas/RegistroWidget.dart';
import 'vistas/PrincipalWidget.dart';

import 'config/globales.dart' as globales;

void main() => runApp(FirebaseApp());

class FirebaseApp extends StatelessWidget {
  @override
  Widget build(BuildContext contexto) {
    return MaterialApp(
        title: 'Bienvenido',
        theme: new ThemeData(primaryColor: Colors.blue),
        initialRoute: '/',
        routes: {
          '': (BuildContext context)=> PrincipalWidget(),
          '/usuarios' :(BuildContext context)=> UsuariosWidget(),
          '/iniciosesion' :(BuildContext context)=> InicioSesionWidget(),
        },
        home: PrincipalWidget(),

    );
  }
}
